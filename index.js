const express = require('express')
const app = express()
const port =process.env.PORT || 7009

app.use(express.static(__dirname))

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html')
})

app.listen(port, (err) => {
    if (err) {
        console.log("Error Loading Server")
    } else {
        console.log("Server Running on: ",port)
    }
})